# android-s2-demo

Demo App of using S2 as a Live Event Map Tracker (without the Events from the server). In my opinion, S2 is one of the best libraries for spatial indexing, but also lacks from 0 zero documentation. This is an app to demonstrate the capabilities of S2, and show you potential usages.

## Usage

- S2 Library, different levels of S2 Cells and their size, spatial indexing, best practices of usage, and misc usage.
- Google Maps, Markers, Polygons
- Realm Database
- Location
- Local Notifications
- Material Components
- FCM (Not used)
- Volley (Not used)
- Remote Config (Not used)

NOTE: This app uses Pokemon Assets to make the app more lively. This is NOT connected to any servers / api, and is merely used for demonstration purposes. 
