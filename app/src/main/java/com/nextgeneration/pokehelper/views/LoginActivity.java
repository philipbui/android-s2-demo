package com.nextgeneration.pokehelper.views;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.nextgeneration.common.IllegalViewException;
import com.nextgeneration.common.LocationUtil;
import com.nextgeneration.pokehelper.R;
import com.nextgeneration.pokehelper.model.SubscribedCell;
import com.nextgeneration.pokehelper.service.DatabaseHelper;
import com.nextgeneration.pokehelper.util.PrefUtil;


public class LoginActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener, View.OnClickListener {

	// Use EditText for cross compatibility
	private EditText email, password;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_activity);
		email = (EditText) findViewById(R.id.email);
		password = (EditText) findViewById(R.id.password);
		findViewById(R.id.signIn).setOnClickListener(this);
		PrefUtil.getPrefs(getApplicationContext()).registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onDestroy() {
		PrefUtil.getPrefs(getApplicationContext()).unregisterOnSharedPreferenceChangeListener(this);
		super.onDestroy();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.signIn:
				if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText()).matches()) {
					((TextInputLayout) email.getParent()).setError(getString(R.string.invalid_email_address));
					return;
				}
				//TODO: Send to Server
				//NetworkManager.getInstance(getApplicationContext()).register(getApplicationContext(), email.getText().toString(), password.getText().toString());
				Location location = LocationUtil.getLocation(this);
				if (location != null) { // Subscribe to where User is residing.
					DatabaseHelper.insertSubscribedCell(new SubscribedCell(PrefUtil.getS2Level(getApplicationContext()), location));
				}
				PrefUtil.setEmail(getApplicationContext(), email.getText().toString());
				PrefUtil.setLoggedIn(getApplicationContext(), true);
				login();
				break;
			default:
				throw new IllegalViewException(view);
		}
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
		if (PrefUtil.getLoggedIn(getApplicationContext())) { // This will be the initial login method, when the network call changes loginPref to true.
			login();
		}
	}

	private void login() {
		Intent maps = new Intent(this, MapsActivity.class);
		startActivity(maps);
		finish();
		overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
	}
}
