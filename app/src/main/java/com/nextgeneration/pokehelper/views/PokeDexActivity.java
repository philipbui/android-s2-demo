package com.nextgeneration.pokehelper.views;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.github.clans.fab.FloatingActionMenu;
import com.nextgeneration.common.IllegalViewException;
import com.nextgeneration.pokehelper.R;
import com.nextgeneration.pokehelper.util.PrefUtil;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class PokeDexActivity extends BaseActivity implements View.OnClickListener {

	protected static final String STATE = "State";
	protected static final int POKEDEX = 0;
	protected static final int NOTIFICATIONS = 1;
	private static final Set<String> twos = new HashSet<>(Arrays.asList("19", "21", "23", "27", "35", "37", "39",
			"41", "44", "46", "48", "50", "52", "54", "56", "58", "72", "77", "79", "81", "84", "86", "88", "90", "96", "98",
			"100", "102", "104", "109", "111", "116", "118", "120", "129", "133", "138", "140"));
	private static final Set<String> threes = new HashSet<>(Arrays.asList("10", "13", "16", "29", "32", "43", "60", "63", "66", "69", "74", "92", "147"));
	private RecyclerView recyclerView;
	private Set<String> filteredPokemon;
	private int state = 0;
	private FloatingActionMenu menu;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.pokedex_activity);
		state = getIntent().getIntExtra(STATE, 0);
		getPrefs();
		setTitle();
		recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
		recyclerView.setLayoutManager(new GridLayoutManager(this, 5));
		recyclerView.setAdapter(new PokeDexAdapter(this));
		menu = (FloatingActionMenu) findViewById(R.id.fab);
		menu.findViewById(R.id.twos).setOnClickListener(this);
		menu.findViewById(R.id.threes).setOnClickListener(this);
		menu.findViewById(R.id.clear).setOnClickListener(this);
		/*recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        menu.showMenu(true);
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        menu.hideMenu(true);
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        break;
                    default:
                        throw new IllegalStateException();
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });*/
	}

	private void setTitle() { // NOTE: Please fix your Plural system Google!
		final int quantity = filteredPokemon.size();
		switch (state) {
			case POKEDEX:
				setTitle((quantity > 0) ? getResources().getQuantityString(R.plurals.pokedex_title, quantity, quantity) : getResources().getString(R.string.pokedex));
				break;
			case NOTIFICATIONS:
				setTitle((quantity > 0) ? getResources().getQuantityString(R.plurals.notifications_title, quantity, quantity) : getResources().getString(R.string.notifications));
				break;
			default:
				throw new IllegalStateException();
		}
	}

	private void getPrefs() {
		switch (state) {
			case POKEDEX:
				filteredPokemon = PrefUtil.getFilterMap(getApplicationContext());
				break;
			case NOTIFICATIONS:
				filteredPokemon = PrefUtil.getFilterNotifications(getApplicationContext());
				break;
			default:
				throw new IllegalStateException();
		}
	}

	private void setPrefs() {
		switch (state) {
			case POKEDEX:
				PrefUtil.setFilterMap(getApplicationContext(), filteredPokemon);
				break;
			case NOTIFICATIONS:
				PrefUtil.setFilterNotifications(getApplicationContext(), filteredPokemon);
				break;
			default:
				throw new IllegalStateException();
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.twos:
				filteredPokemon.addAll(twos);
				setPrefs();
				setTitle();
				recyclerView.getAdapter().notifyDataSetChanged();
				break;
			case R.id.threes:
				filteredPokemon.addAll(threes);
				setPrefs();
				setTitle();
				recyclerView.getAdapter().notifyDataSetChanged();
				break;
			case R.id.clear:
				filteredPokemon = new HashSet<>();
				setPrefs();
				setTitle();
				recyclerView.getAdapter().notifyDataSetChanged();
				break;
			default:
				throw new IllegalViewException(view);
		}
	}

	private class PokeDexAdapter extends RecyclerView.Adapter<PokeDexAdapter.PokemonView> {

		private final LayoutInflater layoutInflater;
		private Resources resources;
		private Context context;

		private PokeDexAdapter(Context context) {
			this.layoutInflater = LayoutInflater.from(context);
			this.resources = context.getResources();
			this.context = context;
		}

		@Override
		public PokemonView onCreateViewHolder(ViewGroup parent, int viewType) {
			return new PokemonView(layoutInflater.inflate(R.layout.pokedex_item, parent, false));
		}

		@Override
		public void onBindViewHolder(final PokemonView holder, int position) {
			final String pos = String.valueOf(position + 1);
			int pokemonDrawable = resources.getIdentifier("p_" + pos, "drawable", context.getPackageName());

			//requestManager.load(pokemonDrawable).into(holder.icon);
			holder.icon.setImageResource(pokemonDrawable);
			holder.view.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (filteredPokemon.contains(pos)) {
						holder.icon.setAlpha(1f);
						filteredPokemon.remove(pos);
					} else {
						holder.icon.setAlpha(0.54f);
						filteredPokemon.add(pos);
					}
					setTitle();
					setPrefs();
				}
			});
			if (filteredPokemon.contains(pos)) {
				holder.icon.setAlpha(0.54f);
			} else {
				holder.icon.setAlpha(1f);
			}
		}

		@Override
		public int getItemCount() {
			return 151;
		}

		public class PokemonView extends RecyclerView.ViewHolder {
			private View view;
			private ImageView icon;

			public PokemonView(View itemView) {
				super(itemView);
				this.view = itemView;
				this.icon = (ImageView) itemView.findViewById(android.R.id.icon);
			}
		}
	}
}
