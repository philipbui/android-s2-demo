package com.nextgeneration.pokehelper.views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.nextgeneration.common.IllegalMenuException;
import com.nextgeneration.pokehelper.R;
import com.nextgeneration.pokehelper.util.PrefUtil;

public class BaseActivity extends AppCompatActivity {

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menuMap:
				if (this instanceof MapsActivity) {
					return true;
				}
				Intent map = new Intent(this, MapsActivity.class);
				startActivity(map);
				finish();
				return true;
			case R.id.menuPokedex:
				if (this instanceof PokeDexActivity && getIntent().getIntExtra(PokeDexActivity.STATE, 0) == PokeDexActivity.POKEDEX) {
					return true;
				}
				Intent pokedex = new Intent(this, PokeDexActivity.class);
				pokedex.putExtra(PokeDexActivity.STATE, PokeDexActivity.POKEDEX);
				startActivity(pokedex);
				finish();
				return true;
			case R.id.menuNotifications:
				if (this instanceof PokeDexActivity && getIntent().getIntExtra(PokeDexActivity.STATE, 0) == PokeDexActivity.NOTIFICATIONS) {
					return true;
				}
				Intent notifications = new Intent(this, PokeDexActivity.class);
				notifications.putExtra(PokeDexActivity.STATE, PokeDexActivity.NOTIFICATIONS);
				startActivity(notifications);
				finish();
				return true;
			case R.id.menuDeveloper:
				if (this instanceof DevActivity) {
					return true;
				}
				Intent developer = new Intent(this, DevActivity.class);
				startActivity(developer);
				finish();
				return true;
			case R.id.menuLogout:
				Intent logout = new Intent(this, LoginActivity.class);
				startActivity(logout);
				finish();
				PrefUtil.setLoggedIn(getApplicationContext(), false);
				return true;
			default:
				throw new IllegalMenuException(item);
		}
	}
}
