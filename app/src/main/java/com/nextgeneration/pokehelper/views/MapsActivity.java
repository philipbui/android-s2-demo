package com.nextgeneration.pokehelper.views;

import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.common.geometry.S2CellId;
import com.google.common.geometry.S2LatLng;
import com.nextgeneration.common.IllegalDialogChoiceException;
import com.nextgeneration.common.LocationUtil;
import com.nextgeneration.pokehelper.R;
import com.nextgeneration.pokehelper.model.Pokemon;
import com.nextgeneration.pokehelper.model.SubscribedCell;
import com.nextgeneration.pokehelper.service.DatabaseHelper;
import com.nextgeneration.pokehelper.util.PokemonUtil;
import com.nextgeneration.pokehelper.util.PrefUtil;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;
import java.util.Set;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;


public class MapsActivity extends BaseActivity implements OnMapReadyCallback, RealmChangeListener<RealmResults<Pokemon>>,
		GoogleMap.OnMapClickListener, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnPolygonClickListener, GoogleMap.OnMapLongClickListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnCameraIdleListener {

	private GoogleMap mMap;
	private RealmResults<Pokemon> pokemon;
	private RealmResults<SubscribedCell> subscribedCells;
	private int level = 12;
	private static final String TAG = MapsActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.maps_activity);
		// Obtain the SupportMapFragment and get notified when the map is ready to be used.
		subscribedCells = DatabaseHelper.getSubscribedCells();
		level = PrefUtil.getS2Level(getApplicationContext());
		((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.menuMap)).getMapAsync(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.map_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.getItem(0).setEnabled(level < 30);
		menu.getItem(1).setEnabled(level > 0);
		getSupportActionBar().setSubtitle(getString(R.string.level_d, level));
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menuUp:
				level++;
				PrefUtil.setS2Level(getApplicationContext(), level);
				invalidateOptionsMenu();
				return true;
			case R.id.menuDown:
				level--;
				PrefUtil.setS2Level(getApplicationContext(), level);
				invalidateOptionsMenu();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Manipulates the map once available.
	 * This callback is triggered when the map is ready to be used.
	 * This is where we can add markers or lines, add listeners or move the camera.
	 * If Google Play services is not installed on the device, the user will be prompted to install
	 * it inside the SupportMapFragment. This method will only be triggered once the user has
	 * installed Google Play services and returned to the app.
	 */
	@Override
	public void onMapReady(GoogleMap googleMap) {
		mMap = googleMap;
		try {
			mMap.setMyLocationEnabled(true);
		} catch (SecurityException e) {
			LocationUtil.requestFineLocationPermission(this);
		}

		mMap.setOnCameraIdleListener(this);
		mMap.setOnMapClickListener(this);
		mMap.setOnMapLongClickListener(this);

		mMap.setOnMyLocationButtonClickListener(this);
		mMap.setOnPolygonClickListener(this);
		mMap.setOnMarkerClickListener(this);
		Location location = LocationUtil.getLocation(this);
		if (location != null) {
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
					new LatLng(location.getLatitude(), location.getLongitude()), 15));
		} else {
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-33.8688, 151.2093), 15));
		}
		pokemon = DatabaseHelper.getPokemonAsync(); // Gets all Pokemon in last 10 minutes.
		pokemon.addChangeListener(this);
	}

	@Override
	public void onChange(RealmResults<Pokemon> element) {
		getSupportActionBar().setTitle(getString(R.string.map_d_pokemon, pokemon.size()));
		mMap.clear();
		Set<String> filteredPokemon = PrefUtil.getFilterMap(getApplicationContext());
		for (Pokemon pokemon : this.pokemon) {
			if (!filteredPokemon.contains(String.valueOf(pokemon.id))) {
				mMap.addMarker(pokemon.toGoogleMapMarker(getApplicationContext()));
			}

		}
		for (SubscribedCell subscribedCell : subscribedCells) {
			mMap.addPolygon(subscribedCell.toGoogleMapPolygonOptions());
		}
	}

	@Override
	public boolean onMyLocationButtonClick() {
		Location location = LocationUtil.getLocation(this);
		if (location != null) {
			mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
			return true;
		} else {
			Snackbar gpsError = Snackbar.make(findViewById(android.R.id.content), R.string.gps_signal_not_found, Snackbar.LENGTH_SHORT);
			gpsError.getView().setBackgroundResource(android.R.color.holo_red_dark);
			gpsError.show();
			return false;
		}
	}

	@Override
	public void onMapClick(LatLng latLng) {
		SubscribedCell subscribedCell = new SubscribedCell(level, latLng);
		DatabaseHelper.insertSubscribedCell(subscribedCell);
		subscribedCell.subscribeGcm();
		mMap.addPolygon(subscribedCell.toGoogleMapPolygonOptions());
		Snackbar.make(findViewById(android.R.id.content), String.format(getString(R.string.subscribed_to_s_events_d_d), subscribedCell.token, String.valueOf(subscribedCell.id), subscribedCell.originalLat, subscribedCell.originalLng), Snackbar.LENGTH_SHORT).show();
	}

	@Override
	public void onPolygonClick(Polygon polygon) {
		// Gets the Center Point by averaging opposite corners and deleting any cells intersecting.
		LatLng latLng = polygon.getPoints().get(0);
		LatLng latLng2 = polygon.getPoints().get(2);
		double lat = (latLng.latitude + latLng2.latitude) / 2;
		double lng = (latLng.longitude + latLng2.longitude) / 2;
		DatabaseHelper.deleteSubscribedCellsIntersecting(lat, lng);
		polygon.remove();
		S2CellId cellId = S2CellId.fromLatLng(S2LatLng.fromDegrees(lat, lng)).parent(level);
		Snackbar.make(findViewById(android.R.id.content), getString(R.string.un_subscribed_from_s_events, cellId.toToken(), String.valueOf(cellId.id())), Snackbar.LENGTH_SHORT).show();
	}

	@Override
	public void onMapLongClick(final LatLng latLng) {
		new AlertDialog.Builder(this).setItems(R.array.map_choices, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				switch (i) {
					case 0:
						mMap.addCircle(new SubscribedCell(S2CellId.fromLatLng(S2LatLng.fromDegrees(latLng.latitude, latLng.longitude))).toGoogleMapCircles());
						break;
					case 1:
						mMap.addMarker(new MarkerOptions().position(latLng).title(String.format(Locale.ENGLISH, "%.12f, %.12f", latLng.latitude, latLng.longitude)));
						break;
					case 2:
						Pokemon pokemon = new Pokemon(new Random().nextInt(151) + 1, latLng.latitude, latLng.longitude, System.currentTimeMillis());
						DatabaseHelper.insertPokemon(pokemon);
						if (!PrefUtil.getFilterNotifications(getApplicationContext()).contains(String.valueOf(pokemon.id))) {
							pokemon.createNotification(getApplicationContext());
						}
						break;
					case 3:
						ArrayList<SubscribedCell> subscribedCells = new ArrayList<>();
						S2CellId[] edgeNeighbours = new S2CellId[4];
						S2CellId.fromLatLng(S2LatLng.fromDegrees(latLng.latitude, latLng.longitude)).parent(level).getEdgeNeighbors(edgeNeighbours);
						for (S2CellId neighbour : edgeNeighbours) {
							SubscribedCell neighbourCell = new SubscribedCell(neighbour);
							neighbourCell.subscribeGcm();
							mMap.addPolygon(neighbourCell.toGoogleMapPolygonOptions());
							subscribedCells.add(new SubscribedCell(neighbour));
						}
						DatabaseHelper.insertSubscribedCell(subscribedCells);
						break;
					case 4:
						ArrayList<SubscribedCell> subscribedCells2 = new ArrayList<>();
						S2CellId cellId2 = S2CellId.fromLatLng(S2LatLng.fromDegrees(latLng.latitude, latLng.longitude));
						SubscribedCell subscribedCell2 = new SubscribedCell(level, latLng);
						mMap.addPolygon(subscribedCell2.toGoogleMapPolygonOptions());
						subscribedCells2.add(subscribedCell2);

						ArrayList<S2CellId> vertexNeighbours = new ArrayList<>();
						cellId2.getVertexNeighbors(level, vertexNeighbours);
						for (S2CellId neighbour : vertexNeighbours) {
							SubscribedCell neighbourCell = new SubscribedCell(neighbour);
							neighbourCell.subscribeGcm();
							mMap.addPolygon(neighbourCell.toGoogleMapPolygonOptions());
							subscribedCells2.add(new SubscribedCell(neighbour));
						}
						DatabaseHelper.insertSubscribedCell(subscribedCells2);
						break;
					case 5:
						ArrayList<SubscribedCell> subscribedCells3 = new ArrayList<>();
						SubscribedCell subscribedCell3 = new SubscribedCell(level, latLng);
						mMap.addPolygon(subscribedCell3.toGoogleMapPolygonOptions());
						subscribedCells3.add(subscribedCell3);

						S2CellId cellId3 = S2CellId.fromLatLng(S2LatLng.fromDegrees(latLng.latitude, latLng.longitude)).parent(level);
						ArrayList<S2CellId> allNeighbours = new ArrayList<>();
						cellId3.getAllNeighbors(level, allNeighbours);
						for (S2CellId neighbour : allNeighbours) {
							SubscribedCell neighbourCell = new SubscribedCell(neighbour);
							neighbourCell.subscribeGcm();
							mMap.addPolygon(neighbourCell.toGoogleMapPolygonOptions());
							subscribedCells3.add(new SubscribedCell(neighbour));
						}
						DatabaseHelper.insertSubscribedCell(subscribedCells3);
						break;
					default:
						throw new IllegalDialogChoiceException(dialogInterface, i);
				}
			}
		}).show();
	}

	@Override
	public boolean onMarkerClick(Marker marker) {
		final LatLng latLng = marker.getPosition();
		Pokemon pokemon = DatabaseHelper.getPokemonAt(latLng.latitude, latLng.longitude);
		if (pokemon != null) {
			Context applicationContext = getApplicationContext();
			new AlertDialog.Builder(this)
					.setIcon(pokemon.getResourceIcon(applicationContext))
					.setTitle(PokemonUtil.getName(applicationContext, pokemon.id))
					.setMessage(PokemonUtil.getType(getApplicationContext(), pokemon.id))
					.setNegativeButton(R.string.delete, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							switch (i) {
								case AlertDialog.BUTTON_NEGATIVE:
									DatabaseHelper.deletePokemonAt(latLng.latitude, latLng.longitude);
									break;
								default:
									throw new IllegalDialogChoiceException(dialogInterface, i);
							}
						}
					})
					.setCancelable(true)
					.show();
			return true;
		}
		/*Object object = marker.getTag();
		if (object.getClass().equals(Pokemon.class)) {
			Pokemon pokemon = (Pokemon) object;
		}*/
		return false;
	}

	@Override
	public void onCameraIdle() {
		Log.d(TAG, String.valueOf(mMap.getCameraPosition().zoom));
	}
}
