package com.nextgeneration.pokehelper.views;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.nextgeneration.common.LocationUtil;
import com.nextgeneration.pokehelper.R;
import com.nextgeneration.pokehelper.service.RemoteConfigService;
import com.nextgeneration.pokehelper.util.PrefUtil;

import java.util.Random;


public class LoadingActivity extends AppCompatActivity {

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loading_activity);
		String[] loadingTexts = getResources().getStringArray(R.array.loading_text);
		((TextView) findViewById(android.R.id.title)).setText(loadingTexts[new Random().nextInt(loadingTexts.length)]);

		//DatabaseHelper.clearDatabase();
		LocationUtil.checkFineLocationPermission(this);
		startService(new Intent(this, RemoteConfigService.class));
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				if (PrefUtil.getLoggedIn(getApplicationContext())) {
					Intent main = new Intent(LoadingActivity.this, MapsActivity.class);
					startActivity(main);
				} else {
					Intent login = new Intent(LoadingActivity.this, LoginActivity.class);
					startActivity(login);
				}
				finish();
				overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
			}
		}, 2000);

	}
}
