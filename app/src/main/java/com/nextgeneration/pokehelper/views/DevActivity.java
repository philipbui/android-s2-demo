package com.nextgeneration.pokehelper.views;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.common.geometry.S2LatLng;
import com.nextgeneration.common.IllegalMenuException;
import com.nextgeneration.common.IllegalViewException;
import com.nextgeneration.common.LocationUtil;
import com.nextgeneration.pokehelper.R;
import com.nextgeneration.pokehelper.model.Pokemon;
import com.nextgeneration.pokehelper.model.SubscribedCell;
import com.nextgeneration.pokehelper.service.DatabaseHelper;
import com.nextgeneration.pokehelper.util.PokemonUtil;
import com.nextgeneration.pokehelper.util.PrefUtil;

import java.util.Locale;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class DevActivity extends BaseActivity implements View.OnClickListener, AbsListView.MultiChoiceModeListener, AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

	private EditText pokemonId, latitude, longitude, topic;
	private ListView listView;
	private DeveloperAdapter adapter;
	private SparseBooleanArray sparseBooleanArray = new SparseBooleanArray();
	private int level = 30;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.developer_activity);
		level = PrefUtil.getS2Level(getApplicationContext());
		listView = (ListView) findViewById(android.R.id.list);
		View view = getLayoutInflater().inflate(R.layout.developer_header, listView, false);
		pokemonId = (EditText) view.findViewById(R.id.pokemonId);
		latitude = (EditText) view.findViewById(R.id.latitude);
		longitude = (EditText) view.findViewById(R.id.longitude);
		topic = (EditText) view.findViewById(R.id.topic);
		view.findViewById(R.id.send).setOnClickListener(this);
		listView.addHeaderView(view);
		listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
		listView.setMultiChoiceModeListener(this);
		listView.setOnItemClickListener(this);
		listView.setOnItemLongClickListener(this);
		adapter = new DeveloperAdapter(this, DatabaseHelper.getSubscribedCellsAsync());
		listView.setAdapter(adapter);
		Location location = LocationUtil.getLocation(this);
		if (location != null) {
			pokemonId.setText("3");
			latitude.setText(String.valueOf(location.getLatitude()));
			longitude.setText(String.valueOf(location.getLongitude()));
		}
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
			case R.id.send:
				Pokemon pokemon = new Pokemon();
				// Validate PokemonId
				String pokemonId = this.pokemonId.getText().toString();
				if (TextUtils.isEmpty(pokemonId)) {
					this.pokemonId.setError(getString(R.string.pokemon_id_cannot_be_null));
					return;
				} else if (!TextUtils.isDigitsOnly(pokemonId)) {
					this.pokemonId.setError(getString(R.string.pokemon_id_must_be_between__and_151));
					return;
				} else {
					int id = Integer.parseInt(pokemonId);
					if (id < 1 || id > 151) {
						this.pokemonId.setError(getString(R.string.pokemon_id_must_be_between__and_151));
						return;
					} else {
						pokemon.id = id;
					}
				}
				//TODO: Encapsulate Latitude Validation
				String latitude = this.latitude.getText().toString();
				if (TextUtils.isEmpty(latitude)) {
					this.latitude.setError(getString(R.string.latitude_cannot_be_empty));
					return;
				} else if (!latitude.matches("\\-?\\d+(\\.\\d+)*")) {
					this.latitude.setError(getString(R.string.latitude_must_be_digits));
					return;
				} else {
					double lat = Double.parseDouble(latitude);
					if (lat < -90 || lat > 90) {
						this.latitude.setError(getString(R.string.latitude_must_be_between_90_and_90));
						return;
					} else {
						pokemon.lat = lat;
					}
				}
				//TODO: Encapsulate Longitude Validation
				String longitude = this.longitude.getText().toString();
				if (TextUtils.isEmpty(longitude)) {
					this.longitude.setError(getString(R.string.longitude_cannot_be_empty));
					return;
				} else if (!latitude.matches("\\-?\\d+(\\.\\d+)*")) {
					this.longitude.setError(getString(R.string.longitude_must_be_digits));
					return;
				} else {
					double lng = Double.parseDouble(longitude);
					if (lng < -180 || lng > 180) {
						this.longitude.setError(getString(R.string.longitude_must_be_between_180_and_180));
						return;
					} else {
						pokemon.lng = lng;
					}
				}
				String topic = this.topic.getText().toString();
				if (TextUtils.isEmpty(topic) || !DatabaseHelper.hasSubscribedCellTopic(topic)) {
					this.topic.setError(getString(R.string.not_subscribed_to_topic_s, topic));
					return;
				}
				pokemon.timestamp = System.currentTimeMillis();
				DatabaseHelper.insertPokemon(pokemon);
				if (!PrefUtil.getFilterNotifications(getApplicationContext()).contains(String.valueOf(pokemon.id))) {
					pokemon.createNotification(getApplicationContext());
				}
				String name = PokemonUtil.getName(getApplicationContext(), pokemon.id);
				Snackbar.make(findViewById(android.R.id.content), getString(R.string.s_created_successfully_at_s_s, name, latitude, longitude), Snackbar.LENGTH_SHORT).show();
				break;
			default:
				throw new IllegalViewException(view);
		}
	}

	@Override
	public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b) {
		if (b) {
			sparseBooleanArray.put(i, true);
		} else {
			sparseBooleanArray.delete(i);
		}
	}

	@Override
	public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {

		actionMode.getMenuInflater().inflate(R.menu.developer_menu, menu);
		return true;
	}

	@Override
	public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
		menu.findItem(R.id.menuUp).setEnabled(level < 30);
		menu.findItem(R.id.menuDown).setEnabled(level > 0);
		actionMode.setTitle(getResources().getQuantityString(R.plurals.d_item_selected, sparseBooleanArray.size(), sparseBooleanArray.size()));
		actionMode.setSubtitle(getString(R.string.level_d, level));
		return true;
	}

	@Override
	public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
		switch (menuItem.getItemId()) {
			case R.id.menuDelete:
				DatabaseHelper.deleteSubscribedCells(sparseBooleanArray);
				return true;
			case R.id.menuUp:
				level++;
				PrefUtil.setS2Level(getApplicationContext(), level);
				actionMode.invalidate();
				DatabaseHelper.migrateSubscribedCells(level, sparseBooleanArray);
				return true;
			case R.id.menuDown:
				level--;
				PrefUtil.setS2Level(getApplicationContext(), level);
				actionMode.invalidate();
				DatabaseHelper.migrateSubscribedCells(level, sparseBooleanArray);
				return true;
			default:
				throw new IllegalMenuException(menuItem);
		}
	}

	@Override
	public void onDestroyActionMode(ActionMode actionMode) {
		sparseBooleanArray = new SparseBooleanArray();
		adapter.notifyDataSetChanged();
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
		if (i == 0) {
			return true;
		}
		startActionMode(this);
		sparseBooleanArray.put(i - 1, true); // -1 Due to Header
		view.setBackgroundColor(Color.LTGRAY);
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
		if (i == 0) {
			return;
		}
		boolean selected = listView.isItemChecked(i);
		sparseBooleanArray.put(i - 1, selected); // -1 Due to Header
		if (selected) {
			view.setBackgroundColor(Color.LTGRAY);
		} else {
			view.setBackgroundColor(Color.TRANSPARENT);
		}
	}


	private class DeveloperAdapter extends BaseAdapter implements RealmChangeListener<RealmResults<SubscribedCell>> {

		private final LayoutInflater layoutInflater;
		public RealmResults<SubscribedCell> subscribedCells;

		public DeveloperAdapter(Context context, RealmResults<SubscribedCell> subscribedCells) {
			super();
			this.layoutInflater = LayoutInflater.from(context);
			this.subscribedCells = subscribedCells;
			this.subscribedCells.addChangeListener(this);
		}

		@Override
		public void onChange(RealmResults<SubscribedCell> element) {
			this.subscribedCells = element;
			notifyDataSetChanged();
		}

		@Override
		public int getCount() {
			return subscribedCells.size();
		}

		@Override
		public Object getItem(int i) {
			return subscribedCells.get(i);
		}

		@Override
		public long getItemId(int i) {
			return subscribedCells.get(i).id;
		}

		@Override
		public View getView(int i, View view, ViewGroup viewGroup) {
			DeveloperViewHolder viewHolder;
			if (view == null) {
				view = layoutInflater.inflate(R.layout.developer_item, viewGroup, false);
				viewHolder = new DeveloperViewHolder(view);
				view.setTag(viewHolder);
			} else {
				viewHolder = (DeveloperViewHolder) view.getTag();
			}
			SubscribedCell subscribedCell = subscribedCells.get(i);
			S2LatLng[] latLng = subscribedCell.getTopLeftAndBottomRightCorners();
			view.setBackgroundColor(Color.TRANSPARENT);
			viewHolder.text1.setText(String.format(Locale.ENGLISH, "%s (%.6f, %.6f) (%.6f, %.6f)", subscribedCell.token, latLng[0].latDegrees(), latLng[0].lngDegrees(), latLng[1].latDegrees(), latLng[1].lngDegrees()));
			viewHolder.text2.setText(String.valueOf(subscribedCell.id));
			viewHolder.level.setText(String.valueOf(subscribedCell.level));
			return view;
		}

		private class DeveloperViewHolder {

			private TextView text1, text2, level;

			private DeveloperViewHolder(View convertView) {
				this.text1 = (TextView) convertView.findViewById(android.R.id.text1);
				this.text2 = (TextView) convertView.findViewById(android.R.id.text2);
				this.level = (TextView) convertView.findViewById(R.id.level);
			}
		}
	}
}
