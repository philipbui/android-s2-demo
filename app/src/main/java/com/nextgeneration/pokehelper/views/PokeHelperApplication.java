package com.nextgeneration.pokehelper.views;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class PokeHelperApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		Realm.setDefaultConfiguration(new RealmConfiguration.Builder(getApplicationContext()).deleteRealmIfMigrationNeeded().build());
	}
}
