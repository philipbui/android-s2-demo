package com.nextgeneration.pokehelper.util;

import android.content.Context;
import android.text.TextUtils;

import com.nextgeneration.pokehelper.R;

import java.util.Arrays;
import java.util.List;

public class PokemonUtil {

	public static String[] names;
	public static List[] types;

	public static String getName(Context context, int position) {
		if (names == null) {
			loadArrays(context);
		}
		return names[position - 1];
	}

	public static List<String> getTypes(Context context, int position) {
		if (types == null) {
			loadArrays(context);
		}
		return types[position - 1];
	}

	public static String getType(Context context, int position) {
		if (types == null) {
			loadArrays(context);
		}
		return TextUtils.join(", ", types[position - 1]);
	}

	private static synchronized void loadArrays(Context context) {
		String[] pokemon = context.getResources().getStringArray(R.array.pokemon_names);
		names = new String[151];
		types = new List[151];
		for (int i = 0; i < pokemon.length; i++) {
			String pokemonItem = pokemon[i].replace("\\)", "");
			String[] pokemonAttrs = pokemonItem.split("\\(");
			names[i] = pokemonAttrs[0];
			String[] pokemonTypes = pokemonAttrs[1].split(",");
			types[i] = Arrays.asList(pokemonTypes);
		}
	}
}
