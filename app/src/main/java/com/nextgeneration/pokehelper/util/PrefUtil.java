package com.nextgeneration.pokehelper.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.HashSet;
import java.util.Set;

public class PrefUtil {

	public static final String LOGGED_IN = "loggedIn";
	private static final String EMAIL = "email";
	private static final String GCM_ID = "gcmId";
	private static final String S2_LEVEL = "s2_level";
	private static final String FILTER_MAP = "filter_map";
	private static final String FILTER_NOTIFICATIONS = "filter_notifications";

	public static SharedPreferences getPrefs(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context);
	}

	private static SharedPreferences.Editor getPrefsEditor(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context).edit();
	}

	public static void setLoggedIn(Context context, boolean loggedIn) {
		getPrefsEditor(context).putBoolean(LOGGED_IN, loggedIn).apply();
	}

	public static boolean getLoggedIn(Context context) {
		return getPrefs(context).getBoolean(LOGGED_IN, false);
	}

	public static void setEmail(Context context, String email) {
		getPrefsEditor(context).putString(EMAIL, email).apply();
	}

	public static String getEmail(Context context) {
		return getPrefs(context).getString(EMAIL, "");
	}

	public static void setS2Level(Context context, int level) {
		getPrefsEditor(context).putInt(S2_LEVEL, level).apply();
	}

	//TODO: Set this to server level
	public static int getS2Level(Context context) {
		return getPrefs(context).getInt(S2_LEVEL, 12);
	}

	public static void setGcmId(Context context, String gcmId) {
		getPrefsEditor(context).putString(GCM_ID, gcmId).apply();
	}

	public static void getGcmId(Context context) {
		getPrefs(context).getString(GCM_ID, null);
	}

	public static void setFilterMap(Context context, Set<String> filteredPokemon) {
		getPrefsEditor(context).putStringSet(FILTER_MAP, filteredPokemon).apply();
	}

	public static Set<String> getFilterMap(Context context) {
		return getPrefs(context).getStringSet(FILTER_MAP, new HashSet<String>());
	}

	public static void clearFilterMap(Context context) {
		getPrefsEditor(context).remove(FILTER_MAP).apply();
	}

	public static void setFilterNotifications(Context context, Set<String> filteredPokemon) {
		getPrefsEditor(context).putStringSet(FILTER_NOTIFICATIONS, filteredPokemon).apply();
		;
	}

	public static Set<String> getFilterNotifications(Context context) {
		return getPrefs(context).getStringSet(FILTER_NOTIFICATIONS, new HashSet<String>());
	}

	public static void clearFilterNotifications(Context context) {
		getPrefsEditor(context).remove(FILTER_NOTIFICATIONS).apply();
	}
}
