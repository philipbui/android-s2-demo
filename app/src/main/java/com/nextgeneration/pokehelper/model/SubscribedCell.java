package com.nextgeneration.pokehelper.model;

import android.graphics.Color;
import android.location.Location;

import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.common.geometry.S2Cell;
import com.google.common.geometry.S2CellId;
import com.google.common.geometry.S2LatLng;
import com.google.common.geometry.S2Loop;
import com.google.common.geometry.S2Point;
import com.google.firebase.messaging.FirebaseMessaging;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SubscribedCell extends RealmObject {

	@PrimaryKey
	public long id;
	public int level;
	public String token;
	public double originalLat;
	public double originalLng;

	public double topLeftLat;
	public double topLeftLng;
	public double bottomRightLat;
	public double bottomRightLng;

	public SubscribedCell() {

	}

	public SubscribedCell(S2CellId cellId) {
		init(cellId);
	}

	public SubscribedCell(int level, Location location) {
		init(S2CellId.fromPoint(new S2Point(location.getLatitude(), location.getLongitude(), location.getAltitude())).parent(level), location.getLatitude(), location.getLongitude());
	}

	public SubscribedCell(int level, LatLng latLng) {
		init(S2CellId.fromLatLng(S2LatLng.fromDegrees(latLng.latitude, latLng.longitude)).parent(level), latLng.latitude, latLng.longitude);
	}

	public void init(S2CellId cellId) {
		S2LatLng latLng = cellId.toLatLng();
		init(cellId, latLng.latDegrees(), latLng.lngDegrees());
	}

	public void init(S2CellId cellId, double originalLat, double originalLng) {
		this.id = cellId.id();
		this.level = cellId.level();
		this.token = cellId.toToken();
		this.originalLat = originalLat;
		this.originalLng = originalLng;
		S2Cell cell = new S2Cell(S2CellId.fromToken(token));
		S2LatLng topLeft = new S2LatLng(cell.getVertex(3));
		this.topLeftLat = topLeft.latDegrees();
		this.topLeftLng = topLeft.lngDegrees();
		S2LatLng bottomRight = new S2LatLng(cell.getVertex(1));
		this.bottomRightLat = bottomRight.latDegrees();
		this.bottomRightLng = bottomRight.lngDegrees();
	}

	public S2CellId toCellId() {
		return S2CellId.fromToken(token);
	}

	public S2LatLng[] getTopLeftAndBottomRightCorners() {
		return new S2LatLng[]{
				S2LatLng.fromDegrees(topLeftLat, topLeftLng),
				S2LatLng.fromDegrees(bottomRightLat, bottomRightLng)
		};
	}

	public PolygonOptions toGoogleMapPolygonOptions() {
		S2Loop loop = new S2Loop(new S2Cell(S2CellId.fromToken(token)));
		PolygonOptions polygonOptions = new PolygonOptions().clickable(true).strokeColor(Color.argb(30, 0, 0, 255)).fillColor(Color.argb(20, 0, 0, 255));
		for (int i = 0; i < loop.numVertices(); i++) {
			S2LatLng latLng = new S2LatLng(loop.vertex(i));
			polygonOptions.add(new LatLng(latLng.latDegrees(), latLng.lngDegrees()));
		}
		return polygonOptions;
	}

	public CircleOptions toGoogleMapCircles() {
		S2Cell cell = new S2Cell(S2CellId.fromToken(token));
		S2LatLng center = new S2LatLng(cell.getCenter());
		return new CircleOptions().clickable(true).strokeColor(Color.argb(30, 0, 0, 255)).fillColor(Color.argb(20, 0, 0, 255))
				.center(new LatLng(center.latDegrees(), center.lngDegrees())).radius(300 - (level * 10));
	}

	public void migrateToLevel(int level) {
		if (this.level <= level) {
			init(S2CellId.fromToken(token).parent(level), originalLat, originalLng);
		} else {
			init(S2CellId.fromLatLng(S2LatLng.fromDegrees(originalLat, originalLng)).parent(level), originalLat, originalLng);
		}
	}

	public void subscribeGcm() {
		FirebaseMessaging.getInstance().subscribeToTopic(this.token);
	}

	public void unSubscribeGcm() {
		FirebaseMessaging.getInstance().unsubscribeFromTopic(this.token);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		SubscribedCell that = (SubscribedCell) o;

		return id == that.id;
	}

	@Override
	public int hashCode() {
		return (int) (id ^ (id >>> 32));
	}
}
