package com.nextgeneration.pokehelper.model;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateUtils;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.common.geometry.S2LatLng;
import com.google.firebase.crash.FirebaseCrash;
import com.nextgeneration.common.LocationUtil;
import com.nextgeneration.pokehelper.R;
import com.nextgeneration.pokehelper.service.S2Helper;
import com.nextgeneration.pokehelper.util.PokemonUtil;

import io.realm.RealmObject;

public class Pokemon extends RealmObject {

	public int id;
	public double lat;
	public double lng;
	public long timestamp;

	// Used for Realm internally
	public Pokemon() {

	}

	public Pokemon(int id, double lat, double lng, long timestamp) {
		this.id = id;
		this.lat = lat;
		this.lng = lng;
		this.timestamp = timestamp;
	}

	public String toTitle() {
		return DateUtils.getRelativeTimeSpanString(timestamp).toString();
	}

	public int getResourceIcon(Context context) {
		return context.getResources().getIdentifier("p_" + String.valueOf(id), "drawable", context.getPackageName());
	}

	public Bitmap getBitmap(Context context) {
		return BitmapFactory.decodeResource(context.getResources(), getResourceIcon(context));
	}

	public BitmapDescriptor getBitmapDescriptor(Context context) {
		Bitmap icon = getBitmap(context);
		icon = Bitmap.createScaledBitmap(icon, 80, 80, false);
		return BitmapDescriptorFactory.fromBitmap(icon);
	}

	public MarkerOptions toGoogleMapMarker(Context context) {
		return new MarkerOptions().position(new LatLng(lat, lng)).title(toTitle()).icon(getBitmapDescriptor(context)).draggable(false);
	}

	public void createNotification(Context context) {
		Location location = null;
		try {
			location = LocationUtil.getLocation(context);
		} catch (SecurityException e) {
			FirebaseCrash.report(e);
		} finally {
			String text;
			if (location != null) {
				S2LatLng s1 = S2LatLng.fromDegrees(location.getLatitude(), location.getLongitude());
				S2LatLng s2 = S2LatLng.fromDegrees(lat, lng);
				text = context.getString(R.string.s_has_appeared_d_meters_from_you, PokemonUtil.getName(context, id), S2Helper.meterDistanceBetween(s1, s2));
			} else {
				text = context.getString(R.string.s_has_appeared_at_d_d, PokemonUtil.getName(context, id), Math.floor(lat), Math.floor(lng));
			}
			Notification notification = new NotificationCompat.Builder(context)
					.setCategory(NotificationCompat.CATEGORY_EVENT)
					.setLargeIcon(getBitmap(context))
					.setSmallIcon(getResourceIcon(context))
					.setGroup(String.valueOf(id))
					.setContentTitle(context.getString(R.string.a_wild_s_has_appeared, PokemonUtil.getName(context, id)))
					.setContentText(text)
					.build();
			((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).notify(this.id, notification);
		}
	}
}
