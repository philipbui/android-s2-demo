package com.nextgeneration.pokehelper.service.fcm;

import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nextgeneration.pokehelper.model.Pokemon;
import com.nextgeneration.pokehelper.service.DatabaseHelper;

import java.util.ArrayList;
import java.util.Map;

public class FirebaseMessageService extends FirebaseMessagingService {

	@Override
	public void onMessageReceived(RemoteMessage remoteMessage) {
		super.onMessageReceived(remoteMessage);
		String topic = remoteMessage.getFrom();
		if (topic.contains("topic/")) { // Un-subscribes if Database doesn't contain that subscription and skip processing it.
			topic = topic.replace("topic/", "");
			if (!DatabaseHelper.hasSubscribedCellTopic(topic)) {
				FirebaseMessaging.getInstance().unsubscribeFromTopic(topic);
				return;
			}
		}

		Map<String, String> data = remoteMessage.getData();
		if (data.size() > 0) {
			ArrayList<Pokemon> pokemon = new ArrayList<>();
			try {
				int count = 0;
				while (true) {
					String pokemonCount = String.valueOf(count);
					if (data.containsKey(pokemonCount)) {
						pokemon.add(new Pokemon(
								Integer.parseInt(pokemonCount),
								Long.parseLong(data.get(pokemonCount + "lat")),
								Long.parseLong(data.get(pokemonCount + "lng")),
								remoteMessage.getSentTime()));
					} else {
						return;
					}
					count++;
				}
			} catch (Throwable e) {
				FirebaseCrash.report(e);
			} finally {
				DatabaseHelper.insertPokemon(pokemon);
			}
		}
	}

	@Override
	public void onDeletedMessages() {
		super.onDeletedMessages();
	}

	@Override
	public void onMessageSent(String s) {
		super.onMessageSent(s);
	}


}
