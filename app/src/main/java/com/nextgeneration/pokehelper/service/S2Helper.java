package com.nextgeneration.pokehelper.service;


import android.location.Location;

import com.google.common.geometry.S2CellId;
import com.google.common.geometry.S2LatLng;
import com.google.common.geometry.S2Point;

public class S2Helper {

	public static S2CellId toCellId(Location location) {
		if (location != null) {
			return S2CellId.fromPoint(new S2Point(location.getLatitude(), location.getLongitude(), location.getAltitude()));
		} else {
			return null;
		}
	}

	public static int meterDistanceBetween(S2LatLng s1, S2LatLng s2) {
		return (int) Math.ceil(s1.getEarthDistance(s2));
	}
}
