package com.nextgeneration.pokehelper.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.nextgeneration.pokehelper.util.PrefUtil;

public class RemoteConfigService extends IntentService {

	public static final String S2_LEVEL = "S2_LEVEL";

	public RemoteConfigService() {
		super(RemoteConfigService.class.getSimpleName());
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		final FirebaseRemoteConfig remoteConfig = FirebaseRemoteConfig.getInstance();
		remoteConfig.fetch().addOnCompleteListener(new OnCompleteListener<Void>() {
			@Override
			public void onComplete(@NonNull Task<Void> task) {
				remoteConfig.activateFetched();
				int level = (int) Math.floor(remoteConfig.getDouble(S2_LEVEL));
				if (level >= 1) {
					PrefUtil.setS2Level(getApplicationContext(), level);
					DatabaseHelper.migrateSubscribedCells(level);
				}
			}
		});
	}
}
