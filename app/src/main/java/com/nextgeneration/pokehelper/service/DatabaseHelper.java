package com.nextgeneration.pokehelper.service;

import android.util.Log;
import android.util.SparseBooleanArray;

import com.nextgeneration.pokehelper.model.Pokemon;
import com.nextgeneration.pokehelper.model.SubscribedCell;

import java.util.Collection;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;

public class DatabaseHelper {

	public static final long MINUTE = 1000 * 60;
	public static final long HOUR = MINUTE * 60;
	public static final long DAY = HOUR * 24;
	public static final long WEEK = DAY * 7;
	private static final String TAG = DatabaseHelper.class.getSimpleName();

	public static void clearDatabase() {
		Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
			@Override
			public void execute(Realm realm) {
				realm.deleteAll();
			}
		}, onSuccess(Realm.class), onError(Realm.class));
	}

	public static <T extends RealmObject> void insert(T object) {
		Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
			@Override
			public void execute(Realm realm) {

			}
		}, onSuccess(object.getClass()), onError(object.getClass()));
	}

	public static <T extends RealmObject> RealmResults<T> getAll(Class<T> clazz) {
		return Realm.getDefaultInstance().where(clazz).findAll();
	}

	public static RealmResults<Pokemon> getPokemonAsync() {
		return Realm.getDefaultInstance().where(Pokemon.class).findAllAsync();
	}

	public static RealmResults<Pokemon> getPokemonAsync(long beforeTimestamp) {
		return Realm.getDefaultInstance().where(Pokemon.class).lessThan("timestamp", System.currentTimeMillis() - beforeTimestamp).findAllAsync();
	}

	public static Pokemon getPokemonAt(double lat, double lng) {
		return Realm.getDefaultInstance().where(Pokemon.class).equalTo("lat", lat).equalTo("lng", lng).findFirst();
	}

	public static void deletePokemonAt(final double lat, final double lng) {
		Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
			@Override
			public void execute(Realm realm) {
				realm.where(Pokemon.class).equalTo("lat", lat).equalTo("lng", lng).findFirst().deleteFromRealm();
			}
		}, onSuccess(Pokemon.class), onError(Pokemon.class));
	}

	public static void insertPokemon(final Pokemon pokemon) {
		Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
			@Override
			public void execute(Realm realm) {
				realm.copyToRealm(pokemon);
			}
		}, onSuccess(Pokemon.class), onError(Pokemon.class));
	}

	public static void insertPokemon(final Collection<Pokemon> pokemon) {
		Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
			@Override
			public void execute(Realm realm) {
				realm.copyToRealm(pokemon);
			}
		}, onSuccess(Pokemon.class), onError(Pokemon.class));
	}

	public static RealmResults<SubscribedCell> getSubscribedCells() {
		return Realm.getDefaultInstance().where(SubscribedCell.class).findAll();
	}

	public static RealmResults<SubscribedCell> getSubscribedCellsAsync() {
		return Realm.getDefaultInstance().where(SubscribedCell.class).findAllAsync();
	}

	public static boolean hasSubscribedCellTopic(String topic) {
		return Realm.getDefaultInstance().where(SubscribedCell.class).equalTo("token", topic).findFirst() != null;
	}

	public static void insertSubscribedCell(final SubscribedCell subscribedCell) {
		Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
			@Override
			public void execute(Realm realm) {
				realm.copyToRealmOrUpdate(subscribedCell);
			}
		}, onSuccess(SubscribedCell.class), onError(SubscribedCell.class));
	}

	public static void insertSubscribedCell(final Collection<SubscribedCell> subscribedCells) {
		Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
			@Override
			public void execute(Realm realm) {
				realm.copyToRealmOrUpdate(subscribedCells);
			}
		}, onSuccess(SubscribedCell.class), onError(SubscribedCell.class));
	}

	public static void migrateSubscribedCells(final int level) {
		Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
			@Override
			public void execute(Realm realm) {
				RealmResults<SubscribedCell> subscribedCells = realm.where(SubscribedCell.class).findAll();
				Set<SubscribedCell> uniqueSubscribedCells = new HashSet<>(subscribedCells.size());
				for (SubscribedCell subscribedCell : subscribedCells) {
					subscribedCell.unSubscribeGcm();
					subscribedCell.migrateToLevel(level);
					subscribedCell.subscribeGcm();
					uniqueSubscribedCells.add(subscribedCell);
				}
				if (subscribedCells.deleteAllFromRealm()) {
					realm.copyToRealmOrUpdate(uniqueSubscribedCells);
				} else {
					throw new RealmException(String.format("Could not migrate to level %s", String.valueOf(level)));
				}
			}
		}, onSuccess(SubscribedCell.class), onError(SubscribedCell.class));
	}

	public static void migrateSubscribedCells(final int level, final SparseBooleanArray sparseBooleanArray) {
		Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
			@Override
			public void execute(Realm realm) {
				RealmResults<SubscribedCell> subscribedCells = realm.where(SubscribedCell.class).findAll();
				for (int i = 0; i < sparseBooleanArray.size(); i++) {
					if (sparseBooleanArray.valueAt(i)) {
						SubscribedCell subscribedCell = subscribedCells.get(sparseBooleanArray.keyAt(i));
						subscribedCell.unSubscribeGcm();
						subscribedCell.migrateToLevel(level);
						subscribedCell.subscribeGcm();
					}
				}
			}
		}, onSuccess(SubscribedCell.class), onError(SubscribedCell.class));
	}

	public static void deleteSubscribedCellsIntersecting(final double lat, final double lng) {
		Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
			@Override
			public void execute(Realm realm) {
				RealmResults<SubscribedCell> subscribedCells = realm.where(SubscribedCell.class)
						.greaterThanOrEqualTo("topLeftLat", lat).greaterThanOrEqualTo("topLeftLng", lng)
						.lessThanOrEqualTo("bottomRightLat", lat).lessThanOrEqualTo("bottomRightLng", lng)
						.findAll();
				for (SubscribedCell subscribedCell : subscribedCells) {
					subscribedCell.unSubscribeGcm();
				}
				if (!subscribedCells.deleteAllFromRealm()) {
					throw new RealmException(String.format(Locale.ENGLISH, "(%.6f, %.6f) could not find any cells intersecting", lat, lng));
				}
			}
		}, onSuccess(SubscribedCell.class), onError(SubscribedCell.class));
	}

	public static void deleteSubscribedCells(final SparseBooleanArray sparseBooleanArray) {
		Realm.getDefaultInstance().executeTransactionAsync(new Realm.Transaction() {
			@Override
			public void execute(Realm realm) {
				RealmResults<SubscribedCell> subscribedCells = realm.where(SubscribedCell.class).findAll();
				for (int i = 0; i < sparseBooleanArray.size(); i++) {
					if (sparseBooleanArray.valueAt(i)) {
						subscribedCells.deleteFromRealm(sparseBooleanArray.keyAt(i));
					}
				}
			}
		}, onSuccess(SubscribedCell.class), onError(SubscribedCell.class));
	}

	public static Realm.Transaction.OnSuccess onSuccess(final Class clazz) {
		return new Realm.Transaction.OnSuccess() {
			@Override
			public void onSuccess() {
				Log.d(TAG, "Successfully inserted " + clazz.getSimpleName());
			}
		};
	}

	public static Realm.Transaction.OnError onError(final Class clazz) {
		return new Realm.Transaction.OnError() {
			@Override
			public void onError(Throwable error) {
				Log.e(TAG, "Error inserting " + clazz.getSimpleName(), error);
			}
		};
	}
}
