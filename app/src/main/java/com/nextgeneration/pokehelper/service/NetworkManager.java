package com.nextgeneration.pokehelper.service;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.nextgeneration.pokehelper.util.PrefUtil;

import org.json.JSONException;
import org.json.JSONObject;

public class NetworkManager {

	private static NetworkManager sInstance;
	private final Context context;
	private final RequestQueue requestQueue;
	private String URL = null;
	private String REGISTER = "/register";
	private String UPDATE_LAT_LNG = "/latlng";
	private String SCAN = "/scan";

	private NetworkManager(Context context) {
		this.context = context.getApplicationContext();
		requestQueue = Volley.newRequestQueue(this.context);
		FirebaseRemoteConfig.getInstance().fetch().addOnCompleteListener(new OnCompleteListener<Void>() {
			@Override
			public void onComplete(@NonNull Task<Void> task) {
				FirebaseRemoteConfig.getInstance().activateFetched();
				String url = FirebaseRemoteConfig.getInstance().getString("URL");
				if (!TextUtils.isEmpty(url)) {
					URL = url;
				}
				String register = FirebaseRemoteConfig.getInstance().getString("REGISTER");
				if (!TextUtils.isEmpty(register)) {
					REGISTER = register;
				}
				String updateLatLng = FirebaseRemoteConfig.getInstance().getString("UPDATE_LAT_LNG");
				if (!TextUtils.isEmpty(updateLatLng)) {
					UPDATE_LAT_LNG = updateLatLng;
				}
				String scan = FirebaseRemoteConfig.getInstance().getString("SCAN");
				if (!TextUtils.isEmpty(scan)) {
					SCAN = scan;
				}
			}
		});
	}

	public static synchronized NetworkManager getInstance(Context context) {
		if (sInstance == null) {
			sInstance = new NetworkManager(context);
		}
		return sInstance;
	}

	public void register(String email, String password) {
		try {
			JSONObject requestBody = new JSONObject();
			requestBody.put("email", email);
			requestBody.put("password", password);
			requestQueue.add(new JsonObjectRequest(Request.Method.POST, REGISTER, requestBody, new Response.Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					PrefUtil.setLoggedIn(context, true);
				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					//FirebaseCrash.report(error);
				}
			}));
		} catch (JSONException e) {
			FirebaseCrash.report(e);
		}
	}

	public void updateLatLng(double lat, double lng, double alt) {
		try {
			JSONObject requestBody = new JSONObject();
			requestBody.put("email", PrefUtil.getEmail(context));
			requestBody.put("lat", lat);
			requestBody.put("lng", lng);
			requestBody.put("alt", alt);
			requestQueue.add(new JsonObjectRequest(Request.Method.POST, UPDATE_LAT_LNG, requestBody, new Response.Listener<JSONObject>() {
				@Override
				public void onResponse(JSONObject response) {
					try {

						FirebaseMessaging.getInstance().subscribeToTopic(String.valueOf(response.getLong("cellId")));
					} catch (JSONException e) {
						FirebaseCrash.report(e);
					}
				}
			}, new Response.ErrorListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					FirebaseCrash.report(error);
				}
			}));
		} catch (JSONException e) {
			FirebaseCrash.report(e);
		}
	}

	public void scan(double lat, double lng) {
		/*try {
            JSONObject requestBody = new JSONObject();
            requestBody.put("email", PrefUtil.getEmail(context));
            requestBody.put("lat", lat);
            requestBody.put("lng", lng);
            requestQueue.add(new JsonObjectRequest(Request.Method.POST, SCAN, requestBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        FirebaseMessaging.getInstance().subscribeToTopic(String.valueOf(response.getLong("cellId")));
                    } catch (JSONException e) {
                        FirebaseCrash.report(e);
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    FirebaseCrash.report(error);
                }
            }));
        } catch (JSONException e) {
            FirebaseCrash.report(e);
        }*/
	}
}
