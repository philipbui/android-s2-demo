package com.nextgeneration.pokehelper.service.fcm;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.nextgeneration.pokehelper.util.PrefUtil;

public class FirebaseIdService extends FirebaseInstanceIdService {

	@Override
	public void onTokenRefresh() {
		super.onTokenRefresh();
		PrefUtil.setGcmId(getApplicationContext(), FirebaseInstanceId.getInstance().getToken());
	}
}
