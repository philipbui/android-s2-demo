package com.nextgeneration.common;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

public final strictfp class LocationUtil {

	@Nullable
	public static Location getLocation(Activity activity) {
		if (checkFineLocationPermission(activity)) {
			return ((LocationManager) activity.getSystemService(Context.LOCATION_SERVICE)).getLastKnownLocation(LocationManager.GPS_PROVIDER);
		} else {
			return null;
		}
	}

	@Nullable
	public static Location getLocation(Context context) throws SecurityException {
		return ((LocationManager) context.getSystemService(Context.LOCATION_SERVICE)).getLastKnownLocation(LocationManager.GPS_PROVIDER);
	}

	public static boolean checkFineLocationPermission(Activity activity) {
		if (hasFineLocationPermission(activity)) {
			requestFineLocationPermission(activity);
			return false;
		} else {
			return true;
		}
	}

	public static boolean hasFineLocationPermission(Context context) {
		return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED;
	}

	public static void requestFineLocationPermission(Activity activity) {
		ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
	}
}
