package com.nextgeneration.common;


import android.view.View;

public class IllegalViewException extends IllegalArgumentException {

	public IllegalViewException(View view) {
		super("Unhandled View Click for " + view);
	}
}
