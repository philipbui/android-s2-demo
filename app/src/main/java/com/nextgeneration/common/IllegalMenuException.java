package com.nextgeneration.common;

import android.view.MenuItem;

public class IllegalMenuException extends IllegalArgumentException {

	public IllegalMenuException(MenuItem menuItem) {
		super("Unhandled MenuItem click for " + menuItem.getTitle());
	}
}
